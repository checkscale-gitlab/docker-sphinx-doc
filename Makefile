SHELL:=/usr/bin/env bash

.DEFAULT_GOAL := default

.PHONY: setup
setup:
	./bin/setup.sh

.PHONY: build
build:
	./bin/build.sh --debug

.PHONY: quality
quality:
	./tests/test-quality.sh

.PHONY: test
test: build
	./tests/test.sh --debug --meld

.PHONY: default
default: build quality test
